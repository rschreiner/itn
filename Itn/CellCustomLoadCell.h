//
//  CellCustomLoadCell.h
//  Itn
//
//  Created by Rodrigo Schreiner on 5/15/13.
//  Copyright (c) 2013 Rodrigo Schreiner. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
@interface CellCustomLoadCell : UITableViewCell<CLLocationManagerDelegate>{
	CLLocationManager *locationManager;
	UISwitch* currentlyUsedSwitch ;
}
@property (assign) BOOL myBool;
@property (weak, nonatomic) IBOutlet UILabel *loadNumber;
@property (strong, nonatomic) NSString *loadid;

- (IBAction)mySwtich:(id)sender;
@property (strong, nonatomic) NSString *customername;
@property (nonatomic, retain) CLLocationManager *locationManager;
@end
