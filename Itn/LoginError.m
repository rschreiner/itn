//
//  LoginError.m
//  Itn
//
//  Created by Rodrigo Schreiner on 5/17/13.
//  Copyright (c) 2013 Rodrigo Schreiner. All rights reserved.
//

#import "LoginError.h"

@interface LoginError ()

@end

@implementation LoginError

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)BackLogin:(id)sender {
	 [self dismissModalViewControllerAnimated:YES];
}
@end
