//
//  AppDelegate.h
//  Itn
//
//  Created by Rodrigo Schreiner on 5/14/13.
//  Copyright (c) 2013 Rodrigo Schreiner. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) ViewController *viewController;

@end
