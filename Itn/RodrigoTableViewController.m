//
//  RodrigoTableViewController.m
//  Itn
//
//  Created by Rodrigo Schreiner on 5/15/13.
//  Copyright (c) 2013 Rodrigo Schreiner. All rights reserved.
//

#import "RodrigoTableViewController.h"
#import "ViewController.h"

@interface RodrigoTableViewController ()
@end
@implementation RodrigoTableViewController{
	NSArray *searchResults;
}
@synthesize tokenid,loadcounter;
- (void)viewDidLoad {
	
    [super viewDidLoad];
	//self.title=loadcounter;
    [self.tableView registerNib:[UINib nibWithNibName:@"CellCustomLoadCell" bundle:nil] forCellReuseIdentifier:@"CellCustomLoadCell"];
	[UIApplication sharedApplication].networkActivityIndicatorVisible=YES;
	NSString *urlString = [NSString stringWithFormat:@"https://test.itradenetwork.com/secure/mobile/getloads.cfm?uuid=%@",tokenid];
	NSURLRequest* request = [NSURLRequest requestWithURL:[NSURL URLWithString:urlString]];
	[[NSURLConnection alloc] initWithRequest:request delegate:self];
}

- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
    NSPredicate *resultPredicate = [NSPredicate
	predicateWithFormat:@"loadnumber contains[cd] %@",
	searchText];
    
    searchResults = [results filteredArrayUsingPredicate:resultPredicate];
}

-(BOOL)searchDisplayController:(UISearchDisplayController *)controller
shouldReloadTableForSearchString:(NSString *)searchString
{
    [self filterContentForSearchText:searchString
                               scope:[[self.searchDisplayController.searchBar scopeButtonTitles]
                                      objectAtIndex:[self.searchDisplayController.searchBar
                                                     selectedScopeButtonIndex]]];
    
    return YES;
}

- (void)viewWillAppear:(BOOL)animated {
	[self.navigationController setNavigationBarHidden:FALSE];
	
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
	data = [[NSMutableData alloc] init];
	
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)theData
{
	[data appendData:theData];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
	[UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
	//news = [NSJSONSerialization JSONObjectWithData:data options:nil error:nil];
	NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:nil error:nil];
	results = [json objectForKey:@"data"];
	if(results.count == 0){
		UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"ITN"
														  message:@"No loads found."
														 delegate:self
												cancelButtonTitle:@"OK"
												otherButtonTitles:nil];
		[message show];

	}
	[mainTableView reloadData];
	
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
	UIAlertView *errorView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"The download could not finish" delegate:nil cancelButtonTitle:@"Dismiss" otherButtonTitles:nil];
	[errorView show];
	
	[UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
	
}

-(int)numberOfSectionsInTableView:(UITableView *)tableView{
	return 1;
}

-(int)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{	
	if (tableView == self.searchDisplayController.searchResultsTableView) {
        return [searchResults count];
		
    } else {
        return [results count];
        
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
		NSString* numberOfRows = [NSString stringWithFormat:@"My Loads [%i]", results.count];
		UINavigationController *navCon  = (UINavigationController*) [self.navigationController.viewControllers objectAtIndex:1];
		navCon.navigationItem.title = numberOfRows;
		static NSString *CellIdentifier = @"CellCustomLoadCell";
	
		CellCustomLoadCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
		if(cell == nil){
			cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"CellCustomLoadCell"];
		}
		NSDictionary *appsdict = [results objectAtIndex:indexPath.row];
		NSString *id = [appsdict objectForKey:@"loadnumber"];
		NSString *id1 = [appsdict objectForKey:@"title"];
		NSString *load = [NSString stringWithFormat:@"%@",id];
		cell.loadNumber.text=load;
		cell.loadid=id;
	
		//UISwitch *switchview = [[UISwitch alloc] initWithFrame:CGRectZero];
		// cell.accessoryView = switchview;
	
	//[switchview addTarget:self action:@selector(mySwitchValueChanged:) forControlEvents:UIControlEventValueChanged];
	cell.customername=[appsdict objectForKey:@"customer"];
    return cell;
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == [alertView cancelButtonIndex]) {
		ViewController *vc = [[ViewController alloc] initWithNibName:@"ViewController" bundle:nil];
		[self.navigationController pushViewController:vc animated:TRUE];
		
	}
}
/*
- (void)startTracking:(UISwitch*)switchView{
	
	if ([switchView isOn]) {
		[switchView setOn:NO animated:YES];
	} else {
		[switchView setOn:YES animated:YES];
	}
	
}
 */

@end
